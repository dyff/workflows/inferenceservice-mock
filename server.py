# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import random

import fastapi

app = fastapi.FastAPI(title="InferenceService Mock")


@app.get(f"/health")
async def health() -> int:
    return fastapi.status.HTTP_200_OK


@app.get(f"/ready")
async def ready() -> int:
    return fastapi.status.HTTP_200_OK


@app.post(
    f"/v1/generate",
    tags=["openllm"],
    summary="[BentoML/OpenLLM] Generate text in response to a prompt",
    response_description="The generated text",
)
async def openllm_v1_generate(request: fastapi.Request):
    body = await request.json()
    print(body)
    try:
        body["prompt"]
        return fastapi.responses.JSONResponse(
            content={
                "responses": [
                    "/v1/generate: All work and no play makes Jack a dull boy"
                ]
            }
        )
    except:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            detail="expected body like '{\"prompt\": <text>}'",
        )


@app.post(
    f"/generate",
    tags=["vllm"],
    summary="[VLLM] Generate text in response to a prompt",
    response_description="The generated text",
)
async def vllm_generate(request: fastapi.Request):
    body = await request.json()
    print(body)
    responses = [
        "/generate: it was the worst of times.",
        "/generate: it was the blurst of times.",
        "/generate: it was pretty OK, I guess.",
    ]
    try:
        body["prompt"]
        random.shuffle(responses)
        return fastapi.responses.JSONResponse(content={"text": list(responses[:2])})
    except:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            detail="expected body like '{\"prompt\": <text>}'",
        )


def _openai_v1_completions(body):
    print(body)

    completions = [
        "/openai/v1/completions: it was the worst of times.",
        "/openai/v1/completions: it was the blurst of times.",
        "/openai/v1/completions: <think>What does best mean, anyway?</think> it was pretty OK, I guess.",
    ]
    random.shuffle(completions)

    response_json = {
        "id": "cmpl-uqkvlQyYK7bGYrRHQ0eXlWi7",
        "object": "text_completion",
        "created": 1589478378,
        "model": "dyff-mock-inferenceservice",
        "system_fingerprint": "fp_44709d6fcb",
        "choices": [
            {
                "text": completion,
                "index": i,
                "logprobs": None,
                "finish_reason": "length",
            }
            for i, completion in enumerate(completions[:2])
        ],
        "usage": {"prompt_tokens": 5, "completion_tokens": 7, "total_tokens": 12},
    }

    try:
        body["model"]  # exercise input schema
        body["prompt"]  # exercise input schema
        return fastapi.responses.JSONResponse(content=response_json)
    except:
        raise fastapi.HTTPException(
            fastapi.status.HTTP_400_BAD_REQUEST,
            detail="see request schema: https://platform.openai.com/docs/api-reference/completions/create",
        )


@app.post(
    f"/openai/v1/completions",
    tags=["openai"],
    summary="[OpenAI] Emulate the /v1/completions API",
    response_description="The text completion response",
)
async def openai_v1_completions(request: fastapi.Request):
    body = await request.json()
    return _openai_v1_completions(body)


@app.post(
    f"/openai/v1/completions/transient-errors",
    tags=["openai"],
    summary="[OpenAI] Emulate the /v1/completions API, but randomly raise transient errors",
    response_description="The text completion response",
)
async def openai_v1_completions_transient_errors(request: fastapi.Request):
    body = await request.json()

    if random.random() < 0.1:
        status = random.choice(
            [
                fastapi.status.HTTP_404_NOT_FOUND,
                fastapi.status.HTTP_503_SERVICE_UNAVAILABLE,
                fastapi.status.HTTP_504_GATEWAY_TIMEOUT,
            ]
        )
        raise fastapi.HTTPException(status, detail="transient error")
    return _openai_v1_completions(body)


@app.post(
    f"/errors/400",
    tags=["errors"],
    summary="[Errors] Raise a 400 Bad Request error",
    response_description="A 400 Bad Request error",
)
async def errors_400(request: fastapi.Request):
    raise fastapi.HTTPException(
        fastapi.status.HTTP_400_BAD_REQUEST,
    )
